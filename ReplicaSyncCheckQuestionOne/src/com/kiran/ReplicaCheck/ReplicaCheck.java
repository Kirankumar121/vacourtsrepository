package com.kiran.ReplicaCheck;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author knadageri
 *
 */

public class ReplicaCheck {

	private static Properties properties = new Properties();
	private static InputStream inputProperties = null;
	private static String LOCAL_PATH;
	private static String REPLICA_PATH;
	private static String delimiter;
	private static final Logger LOGGER = Logger.getLogger(ReplicaCheck.class.getName());

	public static void main(String[] args) throws IOException {

		inputProperties = new FileInputStream("config.properties");
		properties.load(inputProperties);

		LOCAL_PATH = properties.getProperty("local");
		REPLICA_PATH = properties.getProperty("replica");
		delimiter = properties.getProperty("delimiter");

		// Get all the local and replica records from file into a TreeMap
		TreeMap<String, String> mapLocal = getDataFromFile(LOCAL_PATH.trim());
		TreeMap<String, String> mapReplica = getDataFromFile(REPLICA_PATH.trim());

		/* Pass the tree map to check for discrepancies */
		Map<String, String> map = areEqualKeyValues(mapLocal, mapReplica);

		/*
		 * The for each iterator will display all the discrepancies tables using a
		 * Information logger
		 */
		map.forEach((k, v) -> LOGGER.log(Level.INFO, "Discrepancy Table: " + k + " <> Discrepancy Details: " + v));
	}


	/**
	 * This method returns the loaded map with key and values
	 * 
	 * @param String path 
	 * @return TreeMap<String, String>
	 * @throws IOException
	 */
	private static TreeMap<String, String> getDataFromFile(String path) {

		TreeMap<String, String> map = new TreeMap<String, String>();

		try (Stream<String> lines = Files.lines(Paths.get(path))) {
			lines.forEach(line -> map.put(line.split("\\" + delimiter)[0], line.split("\\" + delimiter)[1]));

		} catch (IOException ex) {

			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}
		return map;
	}


	/**
	 * Returns the HashMap with table names which has got discrepency
	 * 
	 * @param  local, replica
	 * @return map
	 */
	private static Map<String, String> areEqualKeyValues(TreeMap<String, String> local,
			TreeMap<String, String> replica) {
		return local.entrySet().stream().filter(e -> !e.getValue().equals(replica.get(e.getKey())))
				.collect(Collectors.toMap(e -> e.getKey(),
						e -> "LOCAL Table Count: " + e.getValue() + "  REMOTE Table Count: " + replica.get(e.getKey())));
	}

}