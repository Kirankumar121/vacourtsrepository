<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Record</title>
</head>
<body>
	<div align="center">
	<h1>User Record</h1>
	<form action="<%=request.getContextPath()%>/SaveRecord" method="post">
		<table>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="lastname" required="required"></td>
			</tr>
			<tr>
				<td>First Name</td>
				<td><input type="text" name="firstname" required="required"></td>
			</tr>
			<tr>
				<td>Suffix</td>
				<td><input type="text" name="suffix"></td>
			</tr>
			<tr><td><input type="submit" value="Submit" style="text-align: center;"></td></tr>
		</table>
	</form>
	
	<div>${msg}</div>
	</div>
</body>
</html>