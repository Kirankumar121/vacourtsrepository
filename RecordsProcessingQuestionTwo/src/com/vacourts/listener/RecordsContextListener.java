package com.vacourts.listener;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.vacourts.record.RecordConstant;

@WebListener
public class RecordsContextListener implements ServletContextListener { 
	
	private static final Logger LOGGER = Logger.getLogger(RecordsContextListener.class.getName());
	
	 	@Override
	    public void contextInitialized(ServletContextEvent servletContextEvent) {
	 		LOGGER.log( Level.INFO,"Starting Up server");
	        File fileFixed = new File(RecordConstant.FILE_DIR+RecordConstant.FIXED_FILE);
	        File fileDup = new File(RecordConstant.FILE_DIR+RecordConstant.DUP_FILE);
	        File fileLoad = new File(RecordConstant.FILE_DIR+RecordConstant.LOAD_FILE);
	        try {
				fileFixed.createNewFile();
				fileDup.createNewFile();
				fileLoad.createNewFile();
			} catch (IOException e) {
				LOGGER.log( Level.SEVERE,"Exception occorued while creating file");
			}
	        
	    }

	    @Override
	    public void contextDestroyed(ServletContextEvent servletContextEvent) {
	    	try
	        { 
	            Files.deleteIfExists(Paths.get(RecordConstant.FILE_DIR+RecordConstant.FIXED_FILE));
	            Files.deleteIfExists(Paths.get(RecordConstant.FILE_DIR+RecordConstant.DUP_FILE));
	            Files.deleteIfExists(Paths.get(RecordConstant.FILE_DIR+RecordConstant.LOAD_FILE));
	        } 
	        catch(NoSuchFileException e) 
	        { 
	        	LOGGER.log( Level.SEVERE, "No such file/directory exists"); 
	        } 
	        catch(DirectoryNotEmptyException e) 
	        { 
	        	LOGGER.log( Level.SEVERE,"Directory is not empty."); 
	        } 
	        catch(IOException e) 
	        { 
	        	LOGGER.log( Level.SEVERE,"Invalid permissions."); 
	        } 
	          
	        System.out.println("Deletion successful."); 
	    } 
    }

