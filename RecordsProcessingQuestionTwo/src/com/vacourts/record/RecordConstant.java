package com.vacourts.record;

/**
 * @author knadageri
 *
 */

/*
 * This class contains the file path and file names used in the program
 * */

public class RecordConstant {

	public static final String FIXED_FILE = "FIXED.txt";
	public static final String LOAD_FILE = "LOAD.txt";
	public static final String DUP_FILE = "DUPLICATES.txt";
    public static final String FILE_DIR = "C:\\Users\\Kirankumar\\Documents\\Records\\";
}


/*
 * We can make the configurable by reading the properties from a configuration file
 * 
 *  I have implemented this feature in the Question 1
 * 
 * */

