package com.vacourts.record;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author knadageri
 */

public class RecordUtil {
	private static Integer fixed_count = 0;
	private static Integer load_count = 0;
	private static Integer dup_count = 0;
	private static final Logger LOGGER = Logger.getLogger(RecordUtil.class.getName());
	
	public static boolean saveTOFile(String data, String file) {
		LOGGER.log(Level.INFO, "saveTOFile Method called");
		Path path = null;
		boolean save = false;
		try {
			path = Paths.get(RecordConstant.FILE_DIR+file);
			if (Files.notExists(path)) {
				Files.createFile(path);
			}
			List<String> lines = Files.readAllLines(path);
			if (file.equals(RecordConstant.FIXED_FILE) && fixed_count==5) {
				lines.add(" ");
				fixed_count = 0;
			}else if (file.equals(RecordConstant.LOAD_FILE) && load_count==5) {
				lines.add(" ");
				load_count = 0;
			}else if (file.equals(RecordConstant.DUP_FILE) && dup_count==5) {
				lines.add(" ");
				dup_count = 0;
			}
			lines.add(data);
			Files.write(path, lines);
			if (file.equals(RecordConstant.FIXED_FILE)) {
				fixed_count++;
			}else if (file.equals(RecordConstant.LOAD_FILE)) {
				load_count++;
			}else if (file.equals(RecordConstant.DUP_FILE)) {
				dup_count++;
			}
			save = true;
		} catch (Exception e) {
			
			LOGGER.log(Level.SEVERE, "Exception occoured when performing saveToFile Method");
		}
		return save;
	}
	
	/**
	 * Checks for duplicate records and returns true or false
	 */
	
	public static boolean checkDuplicate(String data, String file) throws IOException {
		LOGGER.log(Level.INFO, "checkDuplicate Method called");
		Path path = Paths.get(RecordConstant.FILE_DIR+file);
		if (Files.exists(path)) {
			List<String> lines = Files.readAllLines(path);
			return lines.contains(data);
		}
		return false;
	}

}
