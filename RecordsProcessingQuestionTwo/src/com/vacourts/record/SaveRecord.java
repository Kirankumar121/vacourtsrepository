package com.vacourts.record;

/**
 * @author knadageri
 *
 */

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class SaveRecord
 */
@WebServlet("/SaveRecord")
public class SaveRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger atomicInteger = new AtomicInteger(2199);
	private static final Logger LOGGER = Logger.getLogger(SaveRecord.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveRecord() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.log(Level.INFO, "doPost Method called");
		
		String lastname = request.getParameter("lastname");
		String firstname = request.getParameter("firstname");
		String suffix = request.getParameter("suffix");
		if (!lastname.isEmpty()) {
			lastname = lastname.trim();
		}
		if (!firstname.isEmpty()) {
			firstname = firstname.trim();
		}
		if (!suffix.isEmpty()) {
			suffix = suffix.trim();
		}
		
		//create fix data
		String fixed_data = lastname+", "+firstname;
		if (!suffix.isEmpty()) {
			fixed_data = fixed_data+"; "+suffix;
		}
		
		//load data
		int id = atomicInteger.incrementAndGet();
		String load_data = id+"|"+lastname+"|"+firstname;
		if (!suffix.isEmpty()) {
			load_data = load_data+"|"+suffix;
		}
		boolean saveLoad = RecordUtil.saveTOFile(load_data, RecordConstant.LOAD_FILE);
		 
		if (!saveLoad) {
			request.setAttribute("msg", "Error: Failed to save to LOAD file");
		}else{
			request.setAttribute("msg", "Successfully saved to LOAD file");
		}
		
		//check duplicate
		boolean duplicate = RecordUtil.checkDuplicate(fixed_data, RecordConstant.FIXED_FILE);
		if (duplicate) {
			RecordUtil.saveTOFile(load_data, RecordConstant.DUP_FILE);
		}
		
		//save fixed data
		boolean saveFixed = RecordUtil.saveTOFile(fixed_data, RecordConstant.FIXED_FILE);
		if (!saveFixed) {
			request.setAttribute("msg", "Error: Failed to save to FIXED file");
		}
		
		//Return response to the UI
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
